package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	public void init()throws ServletException {
		System.out.println("***************************");
		System.out.println("Initialize connection to database.");
		System.out.println("***************************");
		
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8405435963527493895L;
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		//The getWriter methods is use to print out information in the browser as a response
		PrintWriter out = res.getWriter();
		
		/*
		 * The Parameter name are define in the form of input field.
		 * The parameter are found in the url query string 
		 * getParameter()
		 * */
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String operation = req.getParameter("operation");
		
		int total = 0;
		
		//The POST request here is implemented in order to perform the operations on the basic calculator
		
		if(operation.equals("add")) {
			
			total = num1 + num2;
				
		}
		else if(operation.equals("subtract")) {
			
			total = num1 - num2;
				
		}
		else if(operation.equals("multiply")) {
			
			total = num1 * num2;
			
		}
		
		else if(operation.equals("divide")) {
			
			total = num1 / num2;
			
		}
		
		out.printf("<p>The two numbers you provided are: " +num1+ " , " +num2 + "</p>");
		out.printf("<p>The operation that you wanted is: " + operation + "</p>");
		out.printf("<p>The result is: " + total + "</p>");
	}
		
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		//The GET request here will provide the instructions needed for using the calculator application 
		
		PrintWriter out = res.getWriter();
		
		out.println("<h1>You are now using the calculator app</h1>");
		out.println("<p>To use the app, input two numbers and an operation.</p>");
		out.println("<p>Hit the submit button after filling in the details.</p>");
		out.println("<p>You will get the result shown in your browser!</p>");
	}
	
	//The destroy method here destroys/terminates the application 
	public void destroy() {
		
		System.out.println("***************************");
		System.out.println("Disconnected from database.");
		System.out.println("***************************");
		
	}
	

}
